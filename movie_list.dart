import 'package:flutter/material.dart';
import 'package:uas/http_helper.dart';
import '../komponen/http_helper.dart'; //1

class MovieList extends StatefulWidget { //2
  const MovieList({Key? key}) : super(key: key);

  @override
  State<MovieList> createState() => _MovieListState();  //3
}

class _MovieListState extends State<MovieList> {
  String result = "" ;
  HttpHelper helper = HttpHelper();

  @override
  Widget build(BuildContext context) {
    helper.getUpcoming().then((value) {
      setState(() {
        result = value;
      });
    });
    return Scaffold(
      appBar: AppBar(
        title: Text('Daftar file'),

      ),
      body:  Text (result),
    );
  }
}
