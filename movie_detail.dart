import 'package:flutter/material.dart';
import 'package:uas/mode/movie.dart';
import '../model/movie.dart';

class MovieDetail extends StatelessWidget{
  final Movie selectedMovie;
  const MovieDetail({Key? key, required this.selectedMovie }) : super (key: key);

  get screenHeight => null;
  @override
  Widget build(BuildContext context) {
    String path;
    if (selectedMovie.posterPath != null) {
      path =
      'https://image.tmdb.org/t/p/w500${selectedMovie.posterPath}';
    } else {
      path = 'https://image.freeimage.com/image/large-previews/5eb/movie-clapboard-1184339.jpg';
    }
    return Scaffold(
        appBar: AppBar(
          title: Text('${selectedMovie.title}'),
        ),
        body: SingleChildScrollView(
          child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(16),
              height: screenHeight / 1.5,
              child: Image.network(path),
            ),
            Text('${selectedMovie.overview}'),
          ],
          ),
        ));
  }
}
